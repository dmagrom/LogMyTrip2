package com.cachirulop.logmytrip.dialog;


import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import java.util.Objects;

/**
 * Created by david on 17/10/15.
 */
public class ListDialog
        extends DialogFragment {
    private int _titleId;

    private int _arrayId;

    private OnListDialogListener _listener;

    private int _defaultItem;

    private int _selectedItem;

    public void setTitleId (int titleId) {
        _titleId = titleId;
    }

    public void setDefaultItem (int defaultItem) {
        _defaultItem = defaultItem;
    }

    public void setListener (OnListDialogListener listener) {
        _listener = listener;
    }

    public void setArrayId (int arrayId) {
        _arrayId = arrayId;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        AlertDialog.Builder builder;
        FragmentActivity activity;

        activity = getActivity ();
        if (activity == null) {
            throw new IllegalArgumentException ("No activity found");
        }

        builder = new AlertDialog.Builder (activity);

        // Set the dialog title
        builder.setTitle (_titleId);

        builder.setSingleChoiceItems (_arrayId, _defaultItem,
                                      (dialog, which) -> {
                                          _selectedItem = which;

                                          if (_listener != null) {
                                              _listener.onSingleItemSelected (_selectedItem);
                                          }

                                          Objects.requireNonNull (getDialog ()).dismiss ();
                                      });
        builder.setPositiveButton (android.R.string.ok, (dialog, id) -> Objects.requireNonNull (getDialog ()).dismiss ());

        builder.setNegativeButton (android.R.string.cancel,
                                   (dialog, which) -> Objects.requireNonNull (getDialog ()).dismiss ());

        return builder.create ();
    }

    public interface OnListDialogListener {
        void onSingleItemSelected (int selectedItem);
    }
}
