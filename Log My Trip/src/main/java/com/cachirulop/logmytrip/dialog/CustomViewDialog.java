package com.cachirulop.logmytrip.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

/**
 * Created by dmagro on 19/10/2015.
 */
public class CustomViewDialog<T>
        extends DialogFragment {
    private int _titleId;
    private int _viewId;
    private View _customView;
    private int _iconId = -1;
    private T _value;
    private boolean _positiveButtonEnabled = true;

    private OnCustomDialogListener<T> _listener;

    public void setListener (OnCustomDialogListener<T> listener) {
        _listener = listener;
    }

    public View getCustomView () {
        return _customView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        AlertDialog.Builder builder;
        LayoutInflater inflater;
        FragmentActivity activity;

        builder = new AlertDialog.Builder (getActivity ());
        builder.setTitle (getTitleId ());
        if (_iconId != -1) {
            builder.setIcon (_iconId);
        }

        activity = getActivity ();
        if (activity != null) {
            inflater = activity.getLayoutInflater ();

            _customView = inflater.inflate (getViewId (), null);
            builder.setView (_customView);

            builder.setPositiveButton (android.R.string.ok, (dialog, which) -> {
                if (_listener != null) {
                    _listener.onPositiveButtonClick (this);
                }

                dialog.dismiss ();
            });

            builder.setNegativeButton (android.R.string.cancel, (dialog, which) -> dialog.dismiss ());

            if (_listener != null) {
                _listener.bindData (this, _customView);
            }
        }

        return builder.create ();
    }

    public void enablePositiveButton () {
        AlertDialog dialog;

        dialog = (AlertDialog) getDialog ();
        if (dialog != null) {
            dialog.getButton (AlertDialog.BUTTON_POSITIVE).setEnabled (true);
        }
        else {
            _positiveButtonEnabled = true;
        }
    }

    public void disablePositiveButton () {
        AlertDialog dialog;

        dialog = (AlertDialog) getDialog ();
        if (dialog != null) {
            dialog.getButton (AlertDialog.BUTTON_POSITIVE).setEnabled (false);
        }
        else {
            _positiveButtonEnabled = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (_positiveButtonEnabled) {
            enablePositiveButton ();
        }
        else {
            disablePositiveButton ();
        }
    }

    public int getTitleId () {
        return _titleId;
    }

    public void setTitleId (int titleId) {
        _titleId = titleId;
    }

    public int getViewId () {
        return _viewId;
    }

    public void setViewId (int viewId) {
        _viewId = viewId;
    }

    public int getIconId () {
        return _iconId;
    }

    public void setIconId (int iconId) {
        _iconId = iconId;
    }

    public T getValue () {
        return _value;
    }

    public void setValue (T value) {
        _value = value;
    }

    public interface OnCustomDialogListener<T> {
        void onPositiveButtonClick (CustomViewDialog<T> dialog);
        void bindData (CustomViewDialog<T> dialog, View v);
    }
}
