package com.cachirulop.logmytrip.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

/**
 * helper for Confirm-Dialog creation
 */
public class ConfirmDialog
        extends DialogFragment {
    private int _titleId;

    private int _messageId;
    private OnConfirmDialogListener _listener;

    @NonNull
    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder (getActivity ());
        builder.setTitle (getTitleId ());
        builder.setMessage (getMessageId ());

        builder.setPositiveButton (android.R.string.ok, (dialog, which) -> {
            if (_listener != null) {
                _listener.onPositiveButtonClick ();
            }

            dialog.dismiss ();
        });

        builder.setNegativeButton (android.R.string.cancel, (dialog, which) -> dialog.dismiss ());

        return builder.create ();
    }

    public void setListener (OnConfirmDialogListener listener) {
        _listener = listener;
    }

    public int getTitleId () {
        return _titleId;
    }

    public void setTitleId (int titleId) {
        _titleId = titleId;
    }

    public int getMessageId () {
        return _messageId;
    }

    public void setMessageId (int messageId) {
        _messageId = messageId;
    }

    public interface OnConfirmDialogListener {
        void onPositiveButtonClick ();

    }
}