package com.cachirulop.logmytrip.manager;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.data.LogMyTripDataHelper;
import com.cachirulop.logmytrip.entity.Trip;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TripManager {
    private static final String CONST_TRIP_TABLE_NAME = "trip";

    public static List<Trip> loadTrips (Context ctx) {
        SQLiteDatabase db = null;
        Cursor c = null;
        List<Trip> result;

        result = new ArrayList<> ();

        try {
            db = new LogMyTripDataHelper (ctx).getReadableDatabase ();

            c = db.rawQuery (ctx.getString (R.string.SQL_get_all_trips), null);

            if (c.moveToFirst ()) {
                while (!c.isAfterLast ()) {
                    result.add (createTripFromCursor (c));

                    c.moveToNext ();
                }
            }

            return result;
        }
        finally {
            if (c != null) {
                c.close ();
            }

            if (db != null) {
                db.close ();
            }
        }
    }

    @SuppressLint ("Range")
    private static Trip createTripFromCursor (Cursor c) {
        Trip result;

        result = new Trip ();

        result.setId (c.getLong (c.getColumnIndex ("id")));
        result.setTitle (c.getString (c.getColumnIndex ("title")));
        result.setDescription (c.getString (c.getColumnIndex ("description")));
        result.setBeginDate (new Date (c.getLong (c.getColumnIndex ("begin_date"))));
        result.setEndDate (new Date (c.getLong (c.getColumnIndex ("end_date"))));

        return result;
    }

    public static void deleteTrip (Context ctx, Trip j) {
        try (SQLiteDatabase db = new LogMyTripDataHelper (ctx).getWritableDatabase ()) {
            db.delete (CONST_TRIP_TABLE_NAME,
                       "id = ?",
                       new String[] { Long.toString (j.getId ()) });
        }
    }

    public static void updateTrip (Context ctx, Trip j) {
        saveTrip (ctx, j, false);
    }

    private static Trip saveTrip (Context ctx, Trip t, boolean isInsert) {
        try (SQLiteDatabase db = new LogMyTripDataHelper (ctx).getWritableDatabase ()) {
            ContentValues values;

            values = new ContentValues ();

            values.put ("title", t.getTitle ());
            values.put ("description", t.getDescription ());
            values.put ("begin_date", t.getBeginDate ().getTime ());
            values.put ("end_date", t.getEndDate ().getTime ());

            if (isInsert) {
                db.insert (CONST_TRIP_TABLE_NAME, null, values);
            }
            else {
                db.update (CONST_TRIP_TABLE_NAME,
                           values,
                           "id = ?", new String[] { Long.toString (t.getId ()) });
            }

            t.setId (getLastTripId (db, ctx));

            return t;
        }
    }

    /**
     * Gets the maximum identifier of the trips table
     *
     * @return the maximum trip identifier
     */
    private static long getLastTripId (SQLiteDatabase db, Context ctx) {
        return new LogMyTripDataHelper (ctx).getLastId (db, CONST_TRIP_TABLE_NAME);
    }

    public static Trip getTrip (Context ctx, long idTrip) {
        try (SQLiteDatabase db = new LogMyTripDataHelper (ctx).getReadableDatabase (); Cursor c = db.query (CONST_TRIP_TABLE_NAME,
                                                                                                            null,
                                                                                                            "id = ?",
                                                                                                            new String[] { Long.toString (idTrip) },
                                                                                                            null,
                                                                                                            null,
                                                                                                            null)) {

            if (c != null && c.moveToFirst ()) {
                return createTripFromCursor (c);
            }
            else {
                return null;
            }
        }
    }

    public static Trip insertTrip (Context ctx, Trip j) {
        return saveTrip (ctx, j, true);
    }
}