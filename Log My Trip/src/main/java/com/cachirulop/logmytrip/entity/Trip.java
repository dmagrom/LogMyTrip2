package com.cachirulop.logmytrip.entity;

import android.content.Context;

import com.cachirulop.logmytrip.helper.FormatHelper;
import com.cachirulop.logmytrip.manager.JourneyManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author david
 */
public class Trip
        implements Serializable {
    private long id;
    private String title;
    private String description;
    private Date beginDate;
    private Date endDate;

    public Trip () {
    }

    public long getId () {
        return id;
    }

    public void setId (long id) {
        this.id = id;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {
        this.title = title;
    }

    public String getDescription () {
        return description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public Date getBeginDate () {
        return beginDate;
    }

    public void setBeginDate (Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate () {
        return endDate;
    }

    public void setEndDate (Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString () {
        return "Trip{" +
               "id=" + id +
               ", title='" + title + '\'' +
               ", description='" + description + '\'' +
               ", beginDate=" + beginDate +
               ", endDate=" + endDate +
               '}';
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass () != o.getClass ()) {
            return false;
        }
        Trip trip = (Trip) o;
        return id == trip.id &&
               Objects.equals (title, trip.title) &&
               Objects.equals (description, trip.description) &&
               Objects.equals (beginDate, trip.beginDate) &&
               Objects.equals (endDate, trip.endDate);
    }

    @Override
    public int hashCode () {
        return Objects.hash (id, title, description, beginDate, endDate);
    }
}
