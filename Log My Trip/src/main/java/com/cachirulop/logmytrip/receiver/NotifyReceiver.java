package com.cachirulop.logmytrip.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.cachirulop.logmytrip.manager.ServiceManager;

import java.util.Objects;

/**
 * Created by david on 11/11/15.
 */
public class NotifyReceiver
        extends BroadcastReceiver {
    public final static String ACTION_STOP_LOG = Objects.requireNonNull (NotifyReceiver.class.getPackage ())
                                                        .getName () + ".STOP_LOG";

    public final static String ACTION_START_LOG = Objects.requireNonNull (NotifyReceiver.class.getPackage ())
                                                         .getName () + ".START_LOG";

    public final static String ACTION_STOP_BLUETOOTH = Objects.requireNonNull (NotifyReceiver.class.getPackage ())
                                                              .getName () +
                                                       ".STOP_BLUETOOTH";

    public void onReceive (Context context, Intent intent) {
        if (ACTION_STOP_LOG.equals (intent.getAction ())) {
            ServiceManager.stopLog (context);
        }
        else if (ACTION_START_LOG.equals (intent.getAction ())) {
            ServiceManager.startLog (context);
        }
        else if (ACTION_STOP_BLUETOOTH.equals (intent.getAction ())) {
            ServiceManager.stopBluetooth (context);
        }
    }
}
