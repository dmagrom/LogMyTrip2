package com.cachirulop.logmytrip.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.fragment.TripDetailFragment;

public class TripDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_trip_detail);
        if (savedInstanceState == null) {
            getSupportFragmentManager ().beginTransaction ()
                                        .replace (R.id.container, TripDetailFragment.newInstance ())
                                        .commit ();
        }
    }
}