package com.cachirulop.logmytrip.activity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.util.AttributeSet;

/**
 * Created by david on 31/01/16.
 */
public class CircleImageView
        extends androidx.appcompat.widget.AppCompatImageView {
    final Path path = new Path ();

    public CircleImageView (Context context) {
        super (context);
    }

    public CircleImageView (Context context, AttributeSet attrs) {
        super (context, attrs);
    }

    public CircleImageView (Context context, AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw (Canvas canvas) {
        // Create a circular path.
        final float width;
        final float height;
        final float halfWidth;
        final float halfHeight;
        final float radius;

        width = getWidth ();
        if (width != 0) {
             halfWidth = width / 2;
        }
        else {
            halfWidth = 0;
        }

        height = getHeight ();
        if (height != 0) {
            halfHeight = height / 2;
        }
        else {
            halfHeight = 0;
        }

        radius = Math.max (halfWidth, halfHeight);
        path.reset ();
        path.addCircle (halfWidth, halfHeight, radius, Path.Direction.CCW);

        canvas.clipPath (path);

        super.onDraw (canvas);
    }
}