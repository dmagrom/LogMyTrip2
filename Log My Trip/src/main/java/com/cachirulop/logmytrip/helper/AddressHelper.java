package com.cachirulop.logmytrip.helper;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.cachirulop.logmytrip.entity.Location;
import com.google.android.gms.common.util.Strings;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AddressHelper {
    public static String getLocationAddressDescription (Context ctx, Location location) {
        Geocoder geocoder;
        List<Address> addresses;
        String result = null;

        if (location == null) {
            return "";
        }

        try {
            geocoder = new Geocoder (ctx, Locale.getDefault ());
            addresses = geocoder.getFromLocation (location.getLatitude (),
                                                  location.getLongitude (),
                                                  1);

            if (addresses.size () > 0) {
                result = addresses.get (0).getAddressLine (0);
            }
        }
        catch (IOException e) {
            LogHelper.e ("Error getting address from location: " + e.getLocalizedMessage (), e);
        }

        if (Strings.isEmptyOrWhitespace (result)) {
            result = location.toString ();
        }

        return result;
    }
}
