package com.cachirulop.logmytrip.helper;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;

/**
 * Created by dmagro on 15/10/2015.
 */
public class LogHelper {
    public static final String LOG_CATEGORY = "com.cachirulop.LOG";
    private static final String LOG_FILE_NAME = "logMyTrip_logs.txt";

    private static final boolean LOG_TO_FILE = false;

    private static final String NEW_LINE = System.getProperty ("line.separator");

    private static File _logFile = null;

    public static void initContext (Context context) {
        createLogFile (context);
    }

    public static void d (String msg) {
        fileLog (msg);
        Log.d (LOG_CATEGORY, msg);
    }

    public static void e (String msg) {
        fileLog (msg);
        Log.e (LOG_CATEGORY, msg);
    }

    public static void e (String msg, Throwable t) {
        fileLog (msg);
        Log.e (LOG_CATEGORY, msg, t);
    }

    public static void fileLog (String msg) {
        fileLog (msg, false, null);
    }

    public static void fileLog (String msg, boolean force, Throwable exception) {
        if ((!LOG_TO_FILE && !force) || (_logFile == null)) {
            return;
        }

        StackTraceElement[] stack;
        StringBuilder fullMsg;

        fullMsg = new StringBuilder ();
        fullMsg.append ("*** ");

        stack = Thread.currentThread ().getStackTrace ();
        if (stack.length > 0) {
            boolean next;

            next = false;
            for (StackTraceElement elem : stack) {
                String method;

                method = String.format ("%s.%s", elem.getClassName (), elem.getMethodName ());
                if ((Logger.class.getCanonicalName () + ".log").equals (method)) {
                    next = true;
                }
                else if (next) {
                    fullMsg.append ("[").append (method).append ("]: ");
                    break;
                }
            }
        }

        fullMsg.append (msg);

        if (exception != null) {
            StringWriter out;


            out = new StringWriter ();
            exception.printStackTrace (new PrintWriter (out));

            fullMsg.append ("\nException: ").append (out);
        }

        final SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss.SSS",
                                                           Locale.getDefault (Locale.Category.FORMAT));

        try {
            final FileWriter fileOut = new FileWriter (_logFile, true);
            fileOut.append (sdf.format (new Date ())).append (" : ").append (String.valueOf (fullMsg)).append (NEW_LINE);
            fileOut.close ();
        }
        catch (final IOException e) {
            e.printStackTrace ();
        }
    }

    @SuppressWarnings ("ConstantConditions")
    private static void createLogFile (Context context) {
        String extStorageState = Environment.getExternalStorageState ();
        if (Environment.MEDIA_MOUNTED.equals (extStorageState)) {
            _logFile = new File (context.getExternalFilesDir (""), LOG_FILE_NAME);
        }
        else {
            _logFile = new File (context.getFilesDir (), LOG_FILE_NAME);
        }

        Log.d (LOG_CATEGORY, "Writing logs in path: " + _logFile);

        if (!_logFile.exists ()) {
            try {
                if (_logFile.getParentFile ().mkdirs ()) {
                    if (!_logFile.createNewFile ()) {
                        Log.e (LOG_CATEGORY, "Error creating log file");
                    }
                }
            }
            catch (final IOException e) {
                Log.e (LOG_CATEGORY, "Error creating log file", e);
            }
        }
        else {
            if (_logFile.length () > (10 * 1024 * 1024)) {
                try {
                    if (!_logFile.delete ()) {
                        throw new IOException ("Can't delete file " + _logFile);
                    }

                    if (!_logFile.createNewFile ()) {
                        throw new IOException ("Can't create file " + _logFile);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace ();
                }
            }
        }
    }

}
