package com.cachirulop.logmytrip.helper;

import android.content.Context;
import android.widget.Toast;

public class ToastHelper {

    public static void showLong (Context ctx, String msg) {
        Toast.makeText (ctx, msg, Toast.LENGTH_LONG).show ();
    }

}
