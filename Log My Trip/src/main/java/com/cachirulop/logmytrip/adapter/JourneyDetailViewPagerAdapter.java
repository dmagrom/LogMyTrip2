package com.cachirulop.logmytrip.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.fragment.TabMapFragment;
import com.cachirulop.logmytrip.fragment.TabStatisticsFragment;

/**
 * Created by hp1 on 21-01-2015.
 */
public class JourneyDetailViewPagerAdapter
        extends FragmentStateAdapter {

    private final CharSequence[] _titles;
    private final Fragment[] _fragments;

    public JourneyDetailViewPagerAdapter (Fragment fragment) {
        super (fragment);

        _titles = new CharSequence[2];
        _titles[0] = fragment.getString (R.string.title_map);
        _titles[1] = fragment.getString (R.string.title_statistics);

        _fragments = new Fragment[2];
        _fragments[0] = new TabMapFragment ();
        _fragments[1] = new TabStatisticsFragment ();
    }

    @NonNull
    @Override
    public Fragment createFragment (int position) {
        return _fragments[position];
    }

    // This method return the Number of tabs for the tabs Strip
    @Override
    public int getItemCount () {
        return _fragments.length;
    }

    public CharSequence getItemTitle (int position) {
        return _titles[position];
    }

    public void setMapType (int mapType) {
        getMapFragment ().setMapType (mapType);
        getStatisticsFragment ().setMapType (mapType);
    }

    private TabMapFragment getMapFragment () {
        return (TabMapFragment) _fragments[0];
    }

    private TabStatisticsFragment getStatisticsFragment () {
        return (TabStatisticsFragment) _fragments[1];
    }
}