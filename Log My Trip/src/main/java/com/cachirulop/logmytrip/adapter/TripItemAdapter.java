package com.cachirulop.logmytrip.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.entity.Trip;
import com.cachirulop.logmytrip.fragment.RecyclerViewItemListener;
import com.cachirulop.logmytrip.manager.TripManager;
import com.cachirulop.logmytrip.viewholder.TripItemViewHolder;

import java.util.ArrayList;
import java.util.List;

public class TripItemAdapter
        extends RecyclerView.Adapter<TripItemViewHolder> {

    private final Context _ctx;
    private final SparseBooleanArray _selectedItems;
    private final TripItemAdapterListener _listener;
    private List<Trip> _items;
    private boolean _actionMode;
    private RecyclerViewItemListener _onTripItemListener;

    public TripItemAdapter (Context ctx, TripItemAdapterListener listener) {
        _ctx = ctx;
        _listener = listener;

        loadItems ();

        _onTripItemListener = null;

        _selectedItems = new SparseBooleanArray ();
    }

    public void loadItems () {
        new Thread (() -> {
            _items = TripManager.loadTrips (_ctx);

            onTripLoadedMainThread ();
        }).start ();
    }

    @SuppressLint ("NotifyDataSetChanged")
    private void onTripLoadedMainThread () {
        Handler main;
        Runnable runInMain;

        main = new Handler (_ctx.getMainLooper ());

        runInMain = () -> {
            notifyDataSetChanged ();

            if (_listener != null) {
                _listener.onTripListLoaded ();
            }
        };

        main.post (runInMain);
    }

    public void setOnTripItemClickListener (RecyclerViewItemListener listener) {
        _onTripItemListener = listener;
    }

    @NonNull
    @Override
    public TripItemViewHolder onCreateViewHolder (@NonNull ViewGroup parent, int viewType) {
        View rowView;

        LayoutInflater inflater = (LayoutInflater) _ctx.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate (R.layout.triplist_item, parent, false);

        return new TripItemViewHolder (_ctx, this, rowView);
    }

    @Override
    public void onBindViewHolder (@NonNull TripItemViewHolder holder, int position) {
        TripItemViewHolder vh;

        vh = holder;

        // Drawable background;
        int background;

        if (_actionMode && isSelected (vh.getLayoutPosition ())) {
            background = R.color.default_background;
        }
        else {
            background = R.color.cardview_light_background;
        }

        // Set data into the view.
        vh.bindView (_items.get (position),
                     _selectedItems.get (position, false), background, _onTripItemListener);
    }

    public boolean isSelected (int pos) {
        return _selectedItems.get (pos, false);
    }

    @Override
    public long getItemId (int position) {
        return _items.get (position).getId ();
    }

    @Override
    public int getItemCount () {
        if (_items == null) {
            return 0;
        }
        else {
            return _items.size ();
        }
    }

    public void toggleSelection (int pos) {
        if (_selectedItems.get (pos, false)) {
            _selectedItems.delete (pos);
        }
        else {
            _selectedItems.put (pos, true);
        }

        notifyItemChanged (pos);

        _onTripItemListener.onRecylcerViewItemsSelectedChange ();
    }

    public void selectAllItems () {
        _selectedItems.clear ();

        for (int i = 0; i < _items.size (); i++) {
            _selectedItems.put (i, true);

            notifyItemChanged (i);
        }

        _onTripItemListener.onRecylcerViewItemsSelectedChange ();
    }

    public void deselectAllItems () {
        for (int i = 0; i < _items.size (); i++) {
            if (isSelected (i)) {
                notifyItemChanged (i);
            }
        }

        _selectedItems.clear ();
        _onTripItemListener.onRecylcerViewItemsSelectedChange ();
    }

    public int getSelectedItemCount () {
        return _selectedItems.size ();
    }

    public List<Trip> getSelectedItems () {
        List<Trip> result;

        result = new ArrayList<> (_selectedItems.size ());

        for (int i = 0; i < _selectedItems.size (); i++) {
            result.add (_items.get (_selectedItems.keyAt (i)));
        }

        return result;
    }

    public boolean isActionMode () {
        return _actionMode;
    }

    @SuppressLint ("NotifyDataSetChanged")
    public void setActionMode (boolean selectionMode) {
        this._actionMode = selectionMode;
        this.notifyDataSetChanged ();
    }

    public void removeItem (Trip t) {
        int pos;

        pos = _items.indexOf (t);
        if (pos != -1) {
            _items.remove (t);
            notifyItemChanged (pos);
        }
    }

    public Trip getItem (int position) {
        return _items.get (position);
    }

    public void reloadItems () {
        if (_items != null) {
            _items.clear ();
        }

        loadItems ();
    }

    public void clearTrips () {
        if (_items != null) {
            _items.clear ();
        }
    }

    public interface TripItemAdapterListener {
        void onTripListLoaded ();
    }
}
