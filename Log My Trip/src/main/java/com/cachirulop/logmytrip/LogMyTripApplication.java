package com.cachirulop.logmytrip;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.cachirulop.logmytrip.helper.LogHelper;

/**
 * Created by dmagro on 16/09/2015.
 */
public class LogMyTripApplication
        extends Application {

    public static void runInMainThread (Context ctx, Runnable r) {
        Handler main;

        main = new Handler (ctx.getMainLooper ());

        main.post (r);
    }

    public void onCreate () {
        super.onCreate ();

        LogHelper.initContext (getApplicationContext ());

        registerGlobalExceptionHandler ();
    }

    private void registerGlobalExceptionHandler () {
        Thread.setDefaultUncaughtExceptionHandler ((paramThread, paramThrowable) -> {
            LogHelper.fileLog ("Catch uncaught exception: " +
                               paramThrowable.getLocalizedMessage (),
                               true,
                               paramThrowable);

            LogHelper.e ("Catch uncaught exception", paramThrowable);

            System.exit (2);
        });
    }

}
