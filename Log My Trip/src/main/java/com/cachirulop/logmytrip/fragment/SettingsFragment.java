package com.cachirulop.logmytrip.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.manager.ServiceManager;
import com.cachirulop.logmytrip.manager.SettingsManager;

public class SettingsFragment
        extends PreferenceFragmentCompat
        implements SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreatePreferences (@Nullable Bundle savedInstanceState, @Nullable String rootKey) {
        // super.onCreatePreferences (savedInstanceState, rootKey);

        // Load the preferences from an XML resource
        addPreferencesFromResource (R.xml.preferences);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);

        ListPreference p;
        Context ctx;

        ctx = getActivity ();

        p = findPreference (SettingsManager.KEY_PREF_GPS_TIME_INTERVAL);
        if (p != null) {
            p.setValue (Integer.toString (SettingsManager.getGpsTimeInterval (ctx)));
        }

        p = findPreference (SettingsManager.KEY_PREF_GPS_ACCURACY);
        if (p != null) {
            p.setValue (Integer.toString (SettingsManager.getGpsAccuracy (ctx)));
        }

        p = findPreference (SettingsManager.KEY_PREF_AUTO_START_MODE);
        if (p != null) {
            if (SettingsManager.isAutoStartOnConnect (ctx)) {
                p.setValue ("0");
            }
            else {
                p.setValue ("1");
            }
        }

        setPrefsDependencies ();

        View v;

        v = getView ();
        if (v != null) {
            v.setFitsSystemWindows (true);
        }
    }

    public void setPrefsDependencies () {
        Preference pref;

        pref = findPreference (SettingsManager.KEY_PREF_AUTO_START_LOG_ALWAYS);
        if (pref != null) {
            pref.setEnabled (!SettingsManager.isAutoStartLogBluetooth (this.getActivity ()));
        }

        pref = findPreference (SettingsManager.KEY_PREF_AUTO_START_LOG_BLUETOOTH);
        if (pref != null) {
            pref.setEnabled (!SettingsManager.isAutoStartLogAlways (this.getActivity ()));
        }
    }


    public void onSharedPreferenceChanged (SharedPreferences sharedPreferences, String key) {
        if (SettingsManager.KEY_PREF_AUTO_START_LOG_BLUETOOTH.equals (key)) {
            if (SettingsManager.isAutoStartLogBluetooth (this.getActivity ())) {
                ServiceManager.startBluetooth (this.getActivity ());
            }
            else {
                ServiceManager.stopBluetooth (this.getActivity ());
            }

            setPrefsDependencies ();
        }
        else if (SettingsManager.KEY_PREF_AUTO_START_LOG_ALWAYS.equals (key)) {
            setPrefsDependencies ();
        }
    }

    @Override
    public void onResume () {
        SharedPreferences prefs;

        super.onResume ();

        prefs = getPreferenceManager ().getSharedPreferences ();
        if (prefs != null) {
            prefs.registerOnSharedPreferenceChangeListener (this);
        }
    }

    @Override
    public void onPause () {
        SharedPreferences prefs;

        super.onPause ();

        prefs = getPreferenceManager ().getSharedPreferences ();
        if (prefs != null) {
            prefs.unregisterOnSharedPreferenceChangeListener (this);
        }
    }
}