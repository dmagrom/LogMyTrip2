package com.cachirulop.logmytrip.fragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.adapter.JourneyStatisticsAdapter;
import com.cachirulop.logmytrip.entity.Journey;
import com.cachirulop.logmytrip.manager.LogMyTripBroadcastManager;
import com.cachirulop.logmytrip.manager.SelectedJourneyHolder;


public class TabStatisticsFragment
        extends Fragment {

    private JourneyStatisticsAdapter _adapter;
    private final BroadcastReceiver _onNewLocationReceiver = new BroadcastReceiver () {
        @SuppressLint ("NotifyDataSetChanged")
        @Override
        public void onReceive (Context context, Intent intent) {
            _adapter.notifyDataSetChanged ();
        }
    };
    private Journey _journey;

    public TabStatisticsFragment () {
        // Required empty public constructor
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);

        _journey = SelectedJourneyHolder.getInstance ().getSelectedJourney ();
    }

    @Override
    public View onCreateView (LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        return inflater.inflate (R.layout.fragment_tab_statistics, container, false);
    }

    @Override
    public void onResume () {
        super.onResume ();

        LogMyTripBroadcastManager.registerNewLocationReceiver (getContext (),
                                                               _onNewLocationReceiver);
    }

    @Override
    public void onPause () {
        super.onPause ();

        LogMyTripBroadcastManager.unregisterReceiver (getContext (), _onNewLocationReceiver);
    }

    @Override
    public void onViewCreated (@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);

        Context _ctx = getActivity ();
        RecyclerView _recyclerView = view.findViewById (R.id.rvSegments);
        _recyclerView.setLayoutManager (new LinearLayoutManager (_ctx));
        _recyclerView.setHasFixedSize (true);

        _recyclerView.setItemAnimator (new DefaultItemAnimator ());

        _adapter = new JourneyStatisticsAdapter (_ctx, _journey);
        _recyclerView.setAdapter (_adapter);
    }

    public void setMapType (int mapType) {
        if (_adapter != null) {
            _adapter.setMapType (mapType);
        }
    }

}
