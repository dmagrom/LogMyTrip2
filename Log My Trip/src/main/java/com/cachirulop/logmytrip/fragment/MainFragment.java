package com.cachirulop.logmytrip.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cachirulop.logmytrip.LogMyTripApplication;
import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.activity.JourneyDetailActivity;
import com.cachirulop.logmytrip.adapter.JourneyItemAdapter;
import com.cachirulop.logmytrip.dialog.ConfirmDialog;
import com.cachirulop.logmytrip.dialog.CustomViewDialog;
import com.cachirulop.logmytrip.entity.Journey;
import com.cachirulop.logmytrip.helper.DialogHelper;
import com.cachirulop.logmytrip.helper.ExportHelper;
import com.cachirulop.logmytrip.helper.LogHelper;
import com.cachirulop.logmytrip.manager.JourneyManager;
import com.cachirulop.logmytrip.manager.LogMyTripBroadcastManager;
import com.cachirulop.logmytrip.manager.SelectedJourneyHolder;
import com.cachirulop.logmytrip.manager.ServiceManager;
import com.cachirulop.logmytrip.manager.SettingsManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainFragment
        extends Fragment
        implements RecyclerViewItemListener,
                   ActionMode.Callback {
    private boolean _journeysLoaded;

    private boolean _startLog;

    private RecyclerView _recyclerView;

    private JourneyItemAdapter _adapter;
    private final BroadcastReceiver _onLogStartReceiver = new BroadcastReceiver () {
        @Override
        public void onReceive (Context context, Intent intent) {
            if (_journeysLoaded) {
                autoStartLog ();
            }
            else {
                _startLog = true;
            }
        }
    };
    private final BroadcastReceiver _onNewLocationReceiver = new BroadcastReceiver () {
        @Override
        public void onReceive (Context context, Intent intent) {
            if (_journeysLoaded) {
                Journey current;

                current = _adapter.getItem (0);

                current.computeLiveStatistics (getContext ());

                _adapter.notifyItemChanged (0, current);
            }
        }
    };
    private ActionMode _actionMode;
    private FloatingActionButton _fabLog;
    public final BroadcastReceiver _onLogStopReceiver = new BroadcastReceiver () {
        @Override
        public void onReceive (Context context, Intent intent) {
            _adapter.stopLog (LogMyTripBroadcastManager.getTrip (intent));
            refreshFabLog ();
        }
    };
    private boolean _exportFormatIsGPX;

    public MainFragment () {
        _journeysLoaded = false;
        _startLog = false;
    }

    public void reloadJourneys () {
        if (_actionMode == null) {
            _adapter.reloadItems ();
        }
    }

    @Override
    public void onRecyclerViewItemLongClick () {
        if (_actionMode != null) {
            return;
        }

        if (getView () != null) {
            _actionMode = getView ().startActionMode (this);
            updateActionModeTitle ();
        }
    }

    @Override
    public void onRecylcerViewItemsSelectedChange () {
        updateActionModeTitle ();
    }

    private void updateActionModeTitle () {
        if (_actionMode != null) {
            _actionMode.setTitle (getResources ().getQuantityString (R.plurals.title_selected_count,
                                                                     _adapter.getSelectedItemCount (), _adapter.getSelectedItemCount ()));
        }
    }

    @Override
    public void onRecyclerViewItemClick (int position) {
        if (_actionMode != null) {
            updateActionModeTitle ();
        }
        else {
            startDetailActivity (_adapter.getItem (position));
        }
    }

    private void startDetailActivity (Journey t) {
        Intent i;

        i = new Intent (getContext (), JourneyDetailActivity.class);

        SelectedJourneyHolder.getInstance ().setSelectedJourney (t);

        startActivity (i);
    }

    @Override
    public boolean onCreateActionMode (ActionMode mode, Menu menu) {
        _adapter.setActionMode (true);

        MenuInflater inflater = mode.getMenuInflater ();
        inflater.inflate (R.menu.menu_journey_actionmode, menu);

        _fabLog.hide ();

        return true;
    }

    @Override
    public boolean onPrepareActionMode (ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked (ActionMode mode, MenuItem item) {
        int itemId;

        itemId = item.getItemId ();
        if (itemId == R.id.action_delete_selected_journey) {
            deleteSelectedJourneys ();
            return true;
        }
        else if (itemId == R.id.action_select_all_journeys) {
            selectAllJourneys ();
            return true;
        }
        else if (itemId == R.id.action_deselect_all_journeys) {
            deselectAllJourneys ();
            return true;
        }
        else if (itemId == R.id.action_export) {
            exportJourneysDialog ();
            return true;
        }
        else {
            return false;
        }
    }

    private void deleteSelectedJourneys () {
        ConfirmDialog dlg;

        try {

            dlg = new ConfirmDialog ();
            dlg.setTitleId (R.string.title_delete);
            dlg.setMessageId (R.string.msg_delete_confirm);
            dlg.setListener (() -> {
                List<Journey> selectedItems = _adapter.getSelectedItems ();

                for (Journey t : selectedItems) {
                    Context ctx;

                    ctx = getContext ();
                    if (ctx != null) {
                        JourneyManager.deleteJourney (ctx, t);
                        _adapter.removeItem (t);
                    }
                }

                _actionMode.finish ();
            });

            if (getActivity () != null) {
                dlg.show (getActivity ().getSupportFragmentManager (), "deleteJourneys");
            }
        }
        catch (Exception e) {
            LogHelper.d ("Error showing ConfirmDialog: " + e.getLocalizedMessage ());
        }
    }

    private void selectAllJourneys () {
        _adapter.selectAllItems ();
    }

    private void deselectAllJourneys () {
        _adapter.deselectAllItems ();
    }

    private void exportJourneysDialog () {
        final CustomViewDialog<String> dlg;

        dlg = new CustomViewDialog<> ();

        dlg.setTitleId (R.string.title_export);
        dlg.setViewId (R.layout.dialog_export);
        dlg.setListener (new CustomViewDialog.OnCustomDialogListener () {
            @Override
            public void onPositiveButtonClick (CustomViewDialog dialog) {
                RadioButton rb;

                rb = dlg.getCustomView ().findViewById (R.id.rbExportGPX);
                _exportFormatIsGPX = rb.isChecked ();

                exportJourneys ();
            }

            @Override
            public void bindData (CustomViewDialog dialog, View v) {
                RadioButton rb;

                rb = dlg.getCustomView ().findViewById (R.id.rbExportGPX);
                rb.setChecked (true);
            }
        });

        if (getActivity () != null) {
            dlg.show (getActivity ().getSupportFragmentManager (), "exportJourneys");
        }
    }

    private void exportJourneys () {
        // final ProgressDialog progDialog;
        final Context ctx;
        final LinearLayout progressLayout;
        final TextView tvProgressBarTitle;
        final ProgressBar progressBar;
        final View view;

        ctx = getContext ();
        view = getView ();
        if (ctx != null && view != null) {
            progressLayout = view.findViewById (R.id.progressBarLayout);
            tvProgressBarTitle = view.findViewById (R.id.tvProgressBarTitle);
            progressBar = view.findViewById (R.id.progressBar);

            tvProgressBarTitle.setText (R.string.msg_exporting_journeys);
            progressBar.setIndeterminate (true);
            progressLayout.setVisibility (View.VISIBLE);

            new Thread (() -> {
                List<Journey> selectedItems = _adapter.getSelectedItems ();
                ExportHelper.IExportHelperListener listener;

                listener = new ExportHelper.IExportHelperListener () {
                    @Override
                    public void onExportSuccess (final String fileName) {
                        LogMyTripApplication.runInMainThread (ctx, () -> tvProgressBarTitle.setText (String.format (ctx.getString (R.string.text_file_exported),
                                                                                                            fileName)));
                    }

                    @Override
                    public void onExportFails (int messageId, Object... formatArgs) {
                        DialogHelper.showErrorDialogMainThread (ctx,
                                                                R.string.title_export,
                                                                messageId,
                                                                formatArgs);
                    }
                };

                for (Journey t : selectedItems) {
                    String fileName;

                    if (_exportFormatIsGPX) {
                        fileName = getTrackFileName (t.getJouneyDate (), "gpx");
                    }
                    else {
                        fileName = getTrackFileName (t.getJouneyDate (), "kml");
                    }

                    ExportHelper.exportToFile (ctx, t, fileName, listener);
                }

                LogMyTripApplication.runInMainThread (ctx, () -> {
                    _actionMode.finish ();
                    progressLayout.setVisibility (View.GONE);
                });
            }).start ();
        }
    }

    private String getTrackFileName (Date trackDate, String extension) {
        StringBuilder result;
        DateFormat dfFileName;

        dfFileName = new SimpleDateFormat ("yyyy-MM-dd HHmmss", Locale.getDefault (Locale.Category.FORMAT));

        result = new StringBuilder ();
        result.append (dfFileName.format (trackDate)).append (".").append (extension);

        return result.toString ();
    }

    @Override
    public void onDestroyActionMode (ActionMode mode) {
        _actionMode = null;
        _adapter.setActionMode (false);
        _adapter.deselectAllItems ();
        _fabLog.show ();

        updateActionBarSubtitle ();
    }

    private void updateActionBarSubtitle () {
        ActionBar bar;

        if (getActivity () != null) {
            bar = ((AppCompatActivity) getActivity ()).getSupportActionBar ();
            if (bar != null) {
                Context ctx;

                ctx = getContext ();
                if (ctx != null) {
                    bar.setSubtitle (getContext ().getString (R.string.main_activity_subtitle,
                                                              _adapter.getItemCount ()));
                }
            }
        }
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
    }

    @Override
    public View onCreateView (LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        return inflater.inflate (R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated (@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);

        if (getView () != null) {
            // Recyclerview
            _recyclerView = getView ().findViewById (R.id.rvJourneys);
            _recyclerView.setLayoutManager (new LinearLayoutManager (getContext ()));

            _recyclerView.setItemAnimator (new DefaultItemAnimator ());

            loadJourneys ();
            updateActionBarSubtitle ();

            // Log button
            _fabLog = getView ().findViewById (R.id.fabLog);
            _fabLog.setOnClickListener (v -> onFabLogClick ());

            if (SettingsManager.isLogJourney (getContext ())) {
                // Ensure to start log service
                // if (!LogMyTripService.isRunning ()) {
                ServiceManager.startLog (getContext ());
                // }
            }

            refreshFabLog ();
        }
    }

    @Override
    public void onResume () {
        _adapter.reloadItems ();

        // Receive the broadcast of the LogMyTripService class
        LogMyTripBroadcastManager.registerLogStartReceiver (getContext (), _onLogStartReceiver);
        LogMyTripBroadcastManager.registerLogStopReceiver (getContext (), _onLogStopReceiver);

        LogMyTripBroadcastManager.registerNewLocationReceiver (getContext (),
                                                               _onNewLocationReceiver);

        refreshFabLog ();

        super.onResume ();
    }

    @Override
    public void onPause () {
        if (_actionMode == null) {
            _adapter.clearJourneys ();
            _journeysLoaded = false;
        }

        LogMyTripBroadcastManager.unregisterReceiver (getContext (), _onLogStartReceiver);
        LogMyTripBroadcastManager.unregisterReceiver (getContext (), _onLogStopReceiver);

        LogMyTripBroadcastManager.unregisterReceiver (getContext (), _onNewLocationReceiver);

        super.onPause ();
    }

    /**
     * Load existing journeys
     */
    private void loadJourneys () {
        if (getView () != null) {
            _adapter = new JourneyItemAdapter (getContext (),
                                               () -> {
                                                   updateActionBarSubtitle ();
                                                   _journeysLoaded = true;

                                                   if (_startLog) {
                                                       autoStartLog ();
                                                       _startLog = false;
                                                   }
                                               });

            _adapter.setOnJourneyItemClickListener (this);
            _recyclerView.setAdapter (_adapter);
        }
    }

    private void onFabLogClick () {
        if (SettingsManager.isLogJourney (getContext ())) {
            _recyclerView.scrollToPosition (0);

            ServiceManager.stopLog (getContext ());

            _fabLog.setImageResource (R.mipmap.ic_button_save);
        }
        else {
            ServiceManager.startLog (getContext ());
            _fabLog.setImageResource (android.R.drawable.ic_media_pause);
        }
    }

    private void refreshFabLog () {
        if (SettingsManager.isLogJourney (getContext ())) {
            _fabLog.setImageResource (android.R.drawable.ic_media_pause);
        }
        else {
            _fabLog.setImageResource (R.mipmap.ic_button_save);
        }
    }

    private void autoStartLog () {
        _adapter.startLog ();
        // startDetailActivity (_adapter.getItem (0));
    }

    public void reloadData () {
        reloadJourneys ();
    }
}
