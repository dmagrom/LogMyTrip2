package com.cachirulop.logmytrip.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.adapter.JourneyDetailViewPagerAdapter;
import com.cachirulop.logmytrip.entity.Journey;
import com.cachirulop.logmytrip.manager.SelectedJourneyHolder;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

/**
 * Created by david on 14/09/15.
 */
public class JourneyDetailFragment
        extends Fragment {

    private JourneyDetailViewPagerAdapter _adapter;

    private int _mapType = GoogleMap.MAP_TYPE_NORMAL;

    private Toolbar _toolbar;

    public JourneyDetailFragment () {
    }

    @Override
    public View onCreateView (LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        return inflater.inflate (R.layout.fragment_journey_detail, container, false);
    }

    @Override
    public void onViewCreated (@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);

        ActionBar ab;
        AppCompatActivity app;
        Journey journey;

        journey = SelectedJourneyHolder.getInstance ().getSelectedJourney ();
        app = (AppCompatActivity) getActivity ();

        // Toolbar and back button
        _toolbar = view.findViewById (R.id.journey_detail_toolbar);
        setToolbarTitle (journey.getTitle ());

        assert app != null;
        app.setSupportActionBar (_toolbar);
        ab = app.getSupportActionBar ();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled (true);

        // Adapter
        _adapter = new JourneyDetailViewPagerAdapter (this);

        // ViewPager
        ViewPager2 vpDetailPager = view.findViewById (R.id.vpDetailPager);

        vpDetailPager.setAdapter (_adapter);

        // Tabs
        TabLayout _tabs = view.findViewById (R.id.journey_detail_tablayout);

        new TabLayoutMediator (_tabs,
                               vpDetailPager,
                               (tab, position) -> tab.setText (_adapter.getItemTitle (position))
        ).attach ();
    }

    public void setToolbarTitle (String title) {
        _toolbar.setTitle (title);
    }

    public int getMapType () {
        return _mapType;
    }

    public void setMapType (int type) {
        _mapType = type;
        _adapter.setMapType (type);
    }
}
