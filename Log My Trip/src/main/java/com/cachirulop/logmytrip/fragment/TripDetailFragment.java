package com.cachirulop.logmytrip.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.viewmodel.TripDetailViewModel;

public class TripDetailFragment extends Fragment {

    private TripDetailViewModel mViewModel;

    public static TripDetailFragment newInstance () {
        return new TripDetailFragment ();
    }

    @Override
    public void onCreate (@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        mViewModel = new ViewModelProvider (this).get (TripDetailViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated (@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);

        ActionBar ab;
        AppCompatActivity app;
        Toolbar toolbar;

        app = (AppCompatActivity) getActivity ();

        // Toolbar and back button
        toolbar = view.findViewById (R.id.trip_detail_toolbar);
        toolbar.setTitle ("El titulo");

        assert app != null;
        app.setSupportActionBar (toolbar);
        ab = app.getSupportActionBar ();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled (true);
    }

    @Nullable
    @Override
    public View onCreateView (@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                              @Nullable Bundle savedInstanceState) {
        return inflater.inflate (R.layout.fragment_trip_detail, container, false);
    }

}