package com.cachirulop.logmytrip.fragment;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Pair;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cachirulop.logmytrip.LogMyTripApplication;
import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.activity.SettingsActivity;
import com.cachirulop.logmytrip.activity.TripDetailActivity;
import com.cachirulop.logmytrip.adapter.TripItemAdapter;
import com.cachirulop.logmytrip.dialog.ConfirmDialog;
import com.cachirulop.logmytrip.dialog.CustomViewDialog;
import com.cachirulop.logmytrip.entity.Trip;
import com.cachirulop.logmytrip.helper.DialogHelper;
import com.cachirulop.logmytrip.helper.ExportHelper;
import com.cachirulop.logmytrip.helper.FormatHelper;
import com.cachirulop.logmytrip.helper.LogHelper;
import com.cachirulop.logmytrip.manager.TripManager;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TripsFragment
        extends Fragment
        implements RecyclerViewItemListener,
                   ActionMode.Callback,
                   CustomViewDialog.OnCustomDialogListener<Trip> {
    private boolean _tripsLoaded;
    private boolean _startLog;

    private RecyclerView _recyclerView;

    private TripItemAdapter _adapter;
    private ActionMode _actionMode;
    private FloatingActionButton _fabLog;

    private boolean _exportFormatIsGPX;

    public TripsFragment () {
        _tripsLoaded = false;
        _startLog = false;
    }

    public void reloadTrips () {
        if (_actionMode == null) {
            _adapter.reloadItems ();
        }
    }

    @Override
    public void onRecyclerViewItemLongClick () {
        if (_actionMode != null) {
            return;
        }

        if (getView () != null) {
            _actionMode = getView ().startActionMode (this);
            updateActionModeTitle ();
        }
    }

    @Override
    public void onRecylcerViewItemsSelectedChange () {
        updateActionModeTitle ();
    }

    private void updateActionModeTitle () {
        if (_actionMode != null) {
            _actionMode.setTitle (getResources ().getQuantityString (R.plurals.title_selected_count,
                                                                     _adapter.getSelectedItemCount (), _adapter.getSelectedItemCount ()));
        }
    }

    @Override
    public void onRecyclerViewItemClick (int position) {
        if (_actionMode != null) {
            updateActionModeTitle ();
        }
        else {
            startDetailActivity (_adapter.getItem (position));
        }
    }

    private void startDetailActivity (Trip t) {
/*
        Intent i;

        i = new Intent (getContext (), TripDetailActivity.class);

        SelectedTripHolder.getInstance ().setSelectedTrip (t);

        startActivity (i);
 */
    }

    @Override
    public boolean onCreateActionMode (ActionMode mode, Menu menu) {
        _adapter.setActionMode (true);

        MenuInflater inflater = mode.getMenuInflater ();
        inflater.inflate (R.menu.menu_trip_actionmode, menu);

        _fabLog.hide ();

        return true;
    }

    @Override
    public boolean onPrepareActionMode (ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked (ActionMode mode, MenuItem item) {
        int itemId;

        itemId = item.getItemId ();
        if (itemId == R.id.action_delete_selected_trip) {
            deleteSelectedTrips ();
            return true;
        }
        else if (itemId == R.id.action_select_all_trips) {
            selectAllTrips ();
            return true;
        }
        else if (itemId == R.id.action_deselect_all_trips) {
            deselectAllTrips ();
            return true;
        }
        else if (itemId == R.id.action_export) {
            exportTripsDialog ();
            return true;
        }
        else {
            return false;
        }
    }

    private void deleteSelectedTrips () {
        ConfirmDialog dlg;

        try {

            dlg = new ConfirmDialog ();
            dlg.setTitleId (R.string.title_delete);
            dlg.setMessageId (R.string.msg_delete_confirm);
            dlg.setListener (() -> {
                List<Trip> selectedItems = _adapter.getSelectedItems ();

                for (Trip t : selectedItems) {
                    Context ctx;

                    ctx = getContext ();
                    if (ctx != null) {
                        TripManager.deleteTrip (ctx, t);
                        _adapter.removeItem (t);
                    }
                }

                _actionMode.finish ();
            });

            if (getActivity () != null) {
                dlg.show (getActivity ().getSupportFragmentManager (), "deleteTrips");
            }
        }
        catch (Exception e) {
            LogHelper.d ("Error showing ConfirmDialog: " + e.getLocalizedMessage ());
        }
    }

    private void selectAllTrips () {
        _adapter.selectAllItems ();
    }

    private void deselectAllTrips () {
        _adapter.deselectAllItems ();
    }

    private void exportTripsDialog () {
        final CustomViewDialog dlg;

        dlg = new CustomViewDialog ();

        dlg.setTitleId (R.string.title_export);
        dlg.setViewId (R.layout.dialog_export);
        dlg.setListener (new CustomViewDialog.OnCustomDialogListener () {
            @Override
            public void onPositiveButtonClick (CustomViewDialog dialog) {
                RadioButton rb;

                rb = dlg.getCustomView ().findViewById (R.id.rbExportGPX);
                _exportFormatIsGPX = rb.isChecked ();

                exportTrips ();
            }

            @Override
            public void bindData (CustomViewDialog dialog, View v) {
                RadioButton rb;

                rb = dlg.getCustomView ().findViewById (R.id.rbExportGPX);
                rb.setChecked (true);
            }
        });

        if (getActivity () != null) {
            dlg.show (getActivity ().getSupportFragmentManager (), "exportTrips");
        }
    }

    private void exportTrips () {
        // final ProgressDialog progDialog;
        final Context ctx;
        final LinearLayout progressLayout;
        final TextView tvProgressBarTitle;
        final ProgressBar progressBar;
        final View view;

        ctx = getContext ();
        view = getView ();
        if (ctx != null && view != null) {
            progressLayout = view.findViewById (R.id.progressBarLayout);
            tvProgressBarTitle = view.findViewById (R.id.tvProgressBarTitle);
            progressBar = view.findViewById (R.id.progressBar);

            tvProgressBarTitle.setText (R.string.msg_exporting_trips);
            progressBar.setIndeterminate (true);
            progressLayout.setVisibility (View.VISIBLE);

            new Thread (() -> {
                List<Trip> selectedItems = _adapter.getSelectedItems ();
                ExportHelper.IExportHelperListener listener;

                listener = new ExportHelper.IExportHelperListener () {
                    @Override
                    public void onExportSuccess (final String fileName) {
                        LogMyTripApplication.runInMainThread (ctx, () -> tvProgressBarTitle.setText (String.format (ctx.getString (R.string.text_file_exported),
                                                                                                            fileName)));
                    }

                    @Override
                    public void onExportFails (int messageId, Object... formatArgs) {
                        DialogHelper.showErrorDialogMainThread (ctx,
                                                                R.string.title_export,
                                                                messageId,
                                                                formatArgs);
                    }
                };

                for (Trip t : selectedItems) {
                    String fileName;

                    if (_exportFormatIsGPX) {
                        fileName = getTrackFileName (t, "gpx");
                    }
                    else {
                        fileName = getTrackFileName (t, "kml");
                    }

                    // ExportHelper.exportToFile (ctx, t, fileName, listener);
                }

                LogMyTripApplication.runInMainThread (ctx, () -> {
                    _actionMode.finish ();
                    progressLayout.setVisibility (View.GONE);
                });
            }).start ();
        }
    }

    private String getTrackFileName (Trip trip, String extension) {
        StringBuilder result;
        DateFormat dfFileName;

        dfFileName = new SimpleDateFormat ("yyyy-MM-dd HHmmss", Locale.getDefault (Locale.Category.FORMAT));

        result = new StringBuilder ();
        result.append (dfFileName.format (trip.getBeginDate ())).append ("-").append (trip.getTitle ()).append (".").append (extension);

        return result.toString ();
    }

    @Override
    public void onDestroyActionMode (ActionMode mode) {
        _actionMode = null;
        _adapter.setActionMode (false);
        _adapter.deselectAllItems ();
        _fabLog.show ();

        updateActionBarSubtitle ();
    }

    private void updateActionBarSubtitle () {
        ActionBar bar;

        if (getActivity () != null) {
            bar = ((AppCompatActivity) getActivity ()).getSupportActionBar ();
            if (bar != null) {
                Context ctx;

                ctx = getContext ();
                if (ctx != null) {
                    bar.setSubtitle (getContext ().getString (R.string.main_activity_subtitle,
                                                              _adapter.getItemCount ()));
                }
            }
        }
    }

    @Override
    public View onCreateView (LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        return inflater.inflate (R.layout.fragment_trips, container, false);
    }

    @Override
    public void onViewCreated (@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);

        if (getView () != null) {
            // Recyclerview
            _recyclerView = getView ().findViewById (R.id.rvTrips);
            _recyclerView.setLayoutManager (new LinearLayoutManager (getContext ()));

            _recyclerView.setItemAnimator (new DefaultItemAnimator ());

            loadTrips ();
            updateActionBarSubtitle ();

            // Log button
            _fabLog = getView ().findViewById (R.id.fabLog);
            _fabLog.setOnClickListener (v -> onFabLogClick ());
        }
    }

    @Override
    public void onResume () {
        _adapter.reloadItems ();

        super.onResume ();
    }

    @Override
    public void onPause () {
        if (_actionMode == null) {
            _adapter.clearTrips ();
            _tripsLoaded = false;
        }

        super.onPause ();
    }

    /**
     * Load existing Trips
     */
    private void loadTrips () {
        if (getView () != null) {
            _adapter = new TripItemAdapter (getContext (),
                                               () -> {
                                                   updateActionBarSubtitle ();
                                                   _tripsLoaded = true;
                                               });

            _adapter.setOnTripItemClickListener (this);
            _recyclerView.setAdapter (_adapter);
        }
    }

    private void onFabLogClick () {
        addTrip ();
    }

    private void addTrip () {
        /*
        FragmentActivity activity;

        activity = getActivity ();
        if (activity != null) {
            final CustomViewDialog<Trip> dlg;

            dlg = new CustomViewDialog ();
            dlg.setTitleId (R.string.title_add_trip);
            dlg.setIconId (R.mipmap.ic_trip);
            dlg.setViewId (R.layout.dialog_edit_trip);
            dlg.setListener (this);
            dlg.setValue (new Trip ());
            dlg.disablePositiveButton ();

            dlg.show (activity.getSupportFragmentManager (), "editTrip");
        }
         */
        startActivity (new Intent (getActivity (), TripDetailActivity.class));
    }

    public void reloadData () {
        reloadTrips ();
    }

    @Override
    public void onPositiveButtonClick (CustomViewDialog<Trip> dialog) {
        EditText txt;
        View view;
        Trip trip;

        trip = dialog.getValue ();
        view = dialog.getCustomView ();

        txt = view.findViewById (R.id.etEditTripTitle);

        if (!TextUtils.isEmpty (txt.getText ().toString ())) {
            trip.setTitle (txt.getText ().toString ());
        }
        else {
            txt.setError ("Campo obligatorio");
        }

        txt = view.findViewById (R.id.etEditTripDescription);
        trip.setDescription (txt.getText ().toString ());

        TripManager.insertTrip (this.getContext (), trip);
    }

    @Override
    public void bindData (CustomViewDialog<Trip> dialog, View view) {
        EditText txt;
        Trip trip;
        ImageView ivBeginDate;
        ImageView ivEndDate;

        trip = dialog.getValue ();

        txt = view.findViewById (R.id.etEditTripTitle);
        txt.setText (trip.getTitle ());
        txt.addTextChangedListener (new TextWatcher () {
            @Override
            public void beforeTextChanged (CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged (CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged (Editable s) {

            }
        });

        txt = view.findViewById (R.id.etEditTripDescription);
        txt.setText (trip.getDescription ());

        ivBeginDate = view.findViewById (R.id.ivEditTripBeginDate);
        ivBeginDate.setOnClickListener (v -> {
            DatePickerDialog dateDialog;

            dateDialog = createBeginDateDialog (dialog);

            dateDialog.show ();
        });

        ivEndDate = view.findViewById (R.id.ivEditTripEndDate);
        ivEndDate.setOnClickListener (v -> {
            DatePickerDialog dateDialog;

            dateDialog = createEndDateDialog (dialog);

            dateDialog.show ();
        });
    }

    DatePickerDialog createBeginDateDialog (CustomViewDialog<Trip> dialog) {
        Calendar now;

        now = Calendar.getInstance ();

        return new DatePickerDialog (getContext (), new DatePickerDialog.OnDateSetListener () {
            @Override
            public void onDateSet (DatePicker view, int year, int month, int dayOfMonth) {
                Trip trip;
                Calendar beginDate;

                beginDate = Calendar.getInstance ();
                beginDate.set (Calendar.YEAR, year);
                beginDate.set (Calendar.MONTH, month);
                beginDate.set (Calendar.DAY_OF_MONTH, dayOfMonth);

                trip = dialog.getValue ();
                trip.setBeginDate (beginDate.getTime ());

                TextView txt;

                txt = dialog.getCustomView ().findViewById (R.id.tvEditTripBeginDateValue);
                txt.setText (FormatHelper.formatDate (getContext (), trip.getBeginDate ()));
            }


        }, now.get (Calendar.YEAR), now.get (Calendar.MONTH), now.get (Calendar.DAY_OF_MONTH));
    }

    DatePickerDialog createEndDateDialog (CustomViewDialog<Trip> dialog) {
        Calendar beginDate;
        Trip trip;

        trip = dialog.getValue ();

        beginDate = Calendar.getInstance ();
        if (trip.getBeginDate () != null) {
            beginDate = new Calendar.Builder ().setInstant (trip.getBeginDate ()).build ();
        }
        else {
            beginDate = Calendar.getInstance ();
        }

        return new DatePickerDialog (getContext (), new DatePickerDialog.OnDateSetListener () {
            @Override
            public void onDateSet (DatePicker view, int year, int month, int dayOfMonth) {
                Calendar endCalendar;

                endCalendar = new Calendar.Builder ().setDate (year, month, dayOfMonth).build ();

                trip.setEndDate (endCalendar.getTime ());

                TextView txt;

                txt = dialog.getCustomView ().findViewById (R.id.tvEditTripEndDateValue);
                txt.setText (FormatHelper.formatDate (getContext (), trip.getEndDate ()));
            }
        }, beginDate.get (Calendar.YEAR), beginDate.get (Calendar.MONTH), beginDate.get (Calendar.DAY_OF_MONTH));
    }
}
