package com.cachirulop.logmytrip.fragment;

/**
 * Created by dmagro on 20/10/2015.
 */
public interface RecyclerViewItemListener {
    void onRecyclerViewItemLongClick ();

    void onRecyclerViewItemClick (int position);

    void onRecylcerViewItemsSelectedChange ();
}
