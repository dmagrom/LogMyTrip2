package com.cachirulop.logmytrip.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.adapter.JourneyStatisticsAdapter;
import com.cachirulop.logmytrip.dialog.ConfirmDialog;
import com.cachirulop.logmytrip.entity.JourneySegment;
import com.cachirulop.logmytrip.entity.Location;
import com.cachirulop.logmytrip.helper.AddressHelper;
import com.cachirulop.logmytrip.helper.FormatHelper;
import com.cachirulop.logmytrip.helper.MapHelper;
import com.cachirulop.logmytrip.manager.JourneyManager;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;

public class JourneySegmentViewHolder
        extends RecyclerView.ViewHolder {
    private final JourneyStatisticsAdapter _adapter;
    private final Toolbar _toolbar;
    private final TextView _locationFrom;
    private final TextView _locationTo;
    private final TextView _startDate;
    private final TextView _endDate;
    private final TextView _startTime;
    private final TextView _endTime;
    private final TextView _totalDistance;
    private final TextView _totalTime;
    private final TextView _maxSpeed;
    private final TextView _averageSpeed;
    private final MapView _mapView;
    private final MapHelper _mapHelper;
    private final MapReadyCallback _mapCallback;

    private final Context _ctx;
    private final int _mapType;

    private JourneySegment _segment;

    public JourneySegmentViewHolder (JourneyStatisticsAdapter adapter,
                                     View parent,
                                     Context ctx,
                                     int mapType) {
        super (parent);

        _adapter = adapter;
        _ctx = ctx;
        _mapType = mapType;

        parent.setClickable (false);
        parent.setLongClickable (false);

        GoogleMapOptions options = new GoogleMapOptions ();

        options.liteMode (true);

        _mapHelper = new MapHelper (_ctx);

        _mapView = new MapView (_ctx, options);
        _mapView.onCreate (null);
        _mapView.setClickable (false);

        _mapCallback = new MapReadyCallback ();

        FrameLayout mapFrame = parent.findViewById (R.id.flMapSegment);
        mapFrame.setClickable (false);
        mapFrame.addView (_mapView);

        _locationFrom = parent.findViewById (R.id.tvJourneySegmentLocationFrom);
        _locationTo = parent.findViewById (R.id.tvJourneySegmentLocationTo);

        _startDate = parent.findViewById (R.id.tvJourneySegmentStartDate);
        _endDate = parent.findViewById (R.id.tvJourneySegmentEndDate);
        _startTime = parent.findViewById (R.id.tvJourneySegmentStartTime);
        _endTime = parent.findViewById (R.id.tvJourneySegmentEndTime);
        _totalDistance = parent.findViewById (R.id.tvJourneySegmentTotalDistance);
        _totalTime = parent.findViewById (R.id.tvJourneySegmentTotalTime);
        _maxSpeed = parent.findViewById (R.id.tvJourneySegmentMaxSpeed);
        _averageSpeed = parent.findViewById (R.id.tvJourneySegmentAverageSpeed);

        _toolbar = parent.findViewById (R.id.tbSegmentToolbar);
        _toolbar.inflateMenu (R.menu.menu_segment_actionmode);
    }

    public void bindView (final JourneySegment journeySegment) {
        Location location;

        _segment = journeySegment;  // To get locations

        _toolbar.setTitle (_segment.getTitle (_ctx));
        _toolbar.setOnMenuItemClickListener (item -> {
            int itemId;

            itemId = item.getItemId ();
            if (itemId == R.id.action_delete_segment) {
                ConfirmDialog dlg;

                dlg = new ConfirmDialog ();
                dlg.setTitleId (R.string.title_delete);
                dlg.setMessageId (R.string.msg_delete_confirm);
                dlg.setListener (() -> {
                    _adapter.removeItem (journeySegment);
                    JourneyManager.deleteSegment (_ctx, journeySegment);
                });

                dlg.show (((FragmentActivity) _ctx).getSupportFragmentManager (),
                          "deleteJourney");
            }

            return true;
        });

        location = _segment.getStartLocation ();
        _locationFrom.setText (AddressHelper.getLocationAddressDescription (_ctx, location));
        if (location != null) {
            _startDate.setText (FormatHelper.formatDate (_ctx, location.getLocationTimeAsDate ()));
            _startTime.setText (FormatHelper.formatTime (_ctx, location.getLocationTimeAsDate ()));
        }
        else {
            _startDate.setText ("");
            _startTime.setText ("");
        }

        location = _segment.getEndLocation ();
        _locationTo.setText (AddressHelper.getLocationAddressDescription (_ctx, location));
        if (location != null) {
            _endDate.setText (FormatHelper.formatDate (_ctx, location.getLocationTimeAsDate ()));
            _endTime.setText (FormatHelper.formatTime (_ctx, location.getLocationTimeAsDate ()));
        }
        else {
            _endDate.setText ("");
            _endTime.setText ("");
        }

        _totalDistance.setText (FormatHelper.formatDistance (_segment.computeTotalDistance ()));
        _totalTime.setText (FormatHelper.formatDuration (_segment.computeTotalTime ()));
        _maxSpeed.setText (FormatHelper.formatSpeed (_segment.computeMaxSpeed ()));
        _averageSpeed.setText (FormatHelper.formatSpeed (_segment.computeMediumSpeed ()));

        _mapView.getMapAsync (_mapCallback);
    }

    private class MapReadyCallback
            implements OnMapReadyCallback {
        @Override
        public void onMapReady (GoogleMap googleMap) {
            googleMap.setMapType (_mapType);
            googleMap.clear ();
            googleMap.setOnMapClickListener (latLng -> {
                // Disable map click
            });

            _mapHelper.setMap (googleMap);
            _mapHelper.drawSegment (_segment);
        }
    }
}
