package com.cachirulop.logmytrip.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.adapter.TripItemAdapter;
import com.cachirulop.logmytrip.entity.Trip;
import com.cachirulop.logmytrip.fragment.RecyclerViewItemListener;
import com.cachirulop.logmytrip.helper.FormatHelper;
import com.google.android.gms.common.util.Strings;

/**
 * Created by dmagro on 19/10/2015.
 */
public class TripItemViewHolder
        extends RecyclerView.ViewHolder
        implements View.OnClickListener,
                   View.OnLongClickListener {
    private final Context _ctx;
    private final TripItemAdapter _adapter;
    private final TextView _title;
    private final TextView _description;
    private final TextView _beginDate;
    private final TextView _endDate;
    private RecyclerViewItemListener _onTripItemClickListener;

    public TripItemViewHolder (Context ctx, TripItemAdapter adapter, View parent) {
        super (parent);

        _ctx = ctx;
        _adapter = adapter;

        parent.setClickable (true);
        parent.setLongClickable (true);

        parent.setOnClickListener (this);
        parent.setOnLongClickListener (this);

        RelativeLayout _container = parent.findViewById (R.id.tripListItemContainer);
        _title = parent.findViewById (R.id.tvTripItemTitle);
        _description = parent.findViewById (R.id.tvTripItemDescription);
        _beginDate = parent.findViewById (R.id.tvTripItemBeginDate);
        _endDate = parent.findViewById (R.id.tvTripItemEndDate);

        _onTripItemClickListener = null;

        _container.setClickable (true);
        _container.setLongClickable (true);

        _container.setOnClickListener (this);
        _container.setOnLongClickListener (this);
    }

    public void setOnTripItemClickListener (RecyclerViewItemListener listener) {
        _onTripItemClickListener = listener;
    }

    @Override
    public void onClick (View v) {
        if (_adapter.isActionMode ()) {
            _adapter.toggleSelection (this.getLayoutPosition ());
        }

        if (_onTripItemClickListener != null) {
            _onTripItemClickListener.onRecyclerViewItemClick (this.getBindingAdapterPosition ());
        }
    }

    @Override
    public boolean onLongClick (View v) {
        _adapter.toggleSelection (this.getLayoutPosition ());

        if (_onTripItemClickListener != null) {
            _onTripItemClickListener.onRecyclerViewItemLongClick ();
        }

        return false;
    }

    public void bindView (Trip trip,
                          boolean activated,
                          int background,
                          RecyclerViewItemListener listener) {
        _title.setText (trip.getTitle ());

        if (!Strings.isEmptyOrWhitespace (trip.getDescription ())) {
            _description.setVisibility (View.VISIBLE);
            _description.setText (trip.getDescription ());
        }
        else {
            _description.setVisibility (View.GONE);
        }

        _beginDate.setText (FormatHelper.formatDate (_ctx, trip.getBeginDate ()));
        _endDate.setText (FormatHelper.formatDate (_ctx, trip.getEndDate ()));

        setOnTripItemClickListener (listener);

        itemView.setActivated (activated);

        ((CardView) itemView).setCardBackgroundColor (_ctx.getResources ().getColor (background, null));
    }
}
