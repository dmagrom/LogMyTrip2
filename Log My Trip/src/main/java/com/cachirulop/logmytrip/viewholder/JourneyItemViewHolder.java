package com.cachirulop.logmytrip.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.adapter.JourneyItemAdapter;
import com.cachirulop.logmytrip.entity.Journey;
import com.cachirulop.logmytrip.fragment.RecyclerViewItemListener;
import com.cachirulop.logmytrip.helper.FormatHelper;
import com.cachirulop.logmytrip.manager.SettingsManager;
import com.google.android.gms.common.util.Strings;

/**
 * Created by dmagro on 19/10/2015.
 */
public class JourneyItemViewHolder
        extends RecyclerView.ViewHolder
        implements View.OnClickListener,
                   View.OnLongClickListener {
    private final Context _ctx;
    private final JourneyItemAdapter _adapter;
    private final ImageView _status;
    private final TextView _title;
    private final TextView _description;
    private final TextView _duration;
    private final TextView _date;
    private final TextView _time;
    private RecyclerViewItemListener _onJourneyItemClickListener;

    public JourneyItemViewHolder (Context ctx, JourneyItemAdapter adapter, View parent) {
        super (parent);

        _ctx = ctx;
        _adapter = adapter;

        parent.setClickable (true);
        parent.setLongClickable (true);

        parent.setOnClickListener (this);
        parent.setOnLongClickListener (this);

        RelativeLayout _container = parent.findViewById (R.id.journeyListItemContainer);
        _status = parent.findViewById (R.id.ivJourneyItemStatus);
        _title = parent.findViewById (R.id.tvJourneyItemTitle);
        _description = parent.findViewById (R.id.tvJourneyItemDescription);
        _duration = parent.findViewById (R.id.tvJourneyItemDuration);
        _date = parent.findViewById (R.id.tvJourneyItemDate);
        _time = parent.findViewById (R.id.tvJourneyItemDatetime);

        _onJourneyItemClickListener = null;

        _container.setClickable (true);
        _container.setLongClickable (true);

        _container.setOnClickListener (this);
        _container.setOnLongClickListener (this);
    }

    public void setOnJourneyItemClickListener (RecyclerViewItemListener listener) {
        _onJourneyItemClickListener = listener;
    }

    @Override
    public void onClick (View v) {
        if (_adapter.isActionMode ()) {
            _adapter.toggleSelection (this.getLayoutPosition ());
        }

        if (_onJourneyItemClickListener != null) {
            _onJourneyItemClickListener.onRecyclerViewItemClick (this.getBindingAdapterPosition ());
        }
    }

    @Override
    public boolean onLongClick (View v) {
        _adapter.toggleSelection (this.getLayoutPosition ());

        if (_onJourneyItemClickListener != null) {
            _onJourneyItemClickListener.onRecyclerViewItemLongClick ();
        }

        return false;
    }

    public void bindView (Journey journey,
                          boolean activated,
                          int background,
                          RecyclerViewItemListener listener) {
        int imgId;

        imgId = R.mipmap.ic_journey_status_saved;

        if (SettingsManager.isLogJourney (_ctx)) {
            if (journey.getId () == SettingsManager.getCurrentJourneyId (_ctx)) {
                imgId = R.mipmap.ic_status_logging;
            }
        }

        _status.setImageResource (imgId);
        _title.setText (journey.getTitle ());

        if (!Strings.isEmptyOrWhitespace (journey.getDescription ())) {
            _description.setVisibility (View.VISIBLE);
            _description.setText (journey.getDescription ());
        }
        else {
            _description.setVisibility (View.GONE);
        }

        _duration.setText (journey.getSummary (_ctx));
        _date.setText (FormatHelper.formatDate (_ctx, journey.getJouneyDate ()));
        _time.setText (FormatHelper.formatTime (_ctx, journey.getJouneyDate ()));

        setOnJourneyItemClickListener (listener);

        itemView.setActivated (activated);

        ((CardView) itemView).setCardBackgroundColor (_ctx.getResources ().getColor (background, null));
    }
}
