package com.cachirulop.logmytrip.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cachirulop.logmytrip.R;
import com.cachirulop.logmytrip.entity.Journey;
import com.cachirulop.logmytrip.entity.Location;
import com.cachirulop.logmytrip.helper.AddressHelper;
import com.cachirulop.logmytrip.helper.FormatHelper;

/**
 * Created by dmagro on 06/10/2015.
 */
public class JourneySummaryViewHolder
        extends RecyclerView.ViewHolder {
    private final TextView _description;

    private final TableRow _descriptionRow;

    private final TextView _locationFrom;

    private final TextView _locationTo;

    private final TextView _startDate;

    private final TextView _endDate;

    private final TextView _startTime;

    private final TextView _endTime;

    private final TextView _totalDistance;

    private final TextView _totalTime;

    private final TextView _maxSpeed;

    private final TextView _mediumSpeed;

    private final Context _ctx;

    public JourneySummaryViewHolder (View parent, Context ctx) {
        super (parent);

        _ctx = ctx;

        parent.setClickable (false);
        parent.setLongClickable (false);

        _descriptionRow = parent.findViewById (R.id.tvJourneySummaryDescriptionRow);
        _description = parent.findViewById (R.id.tvJourneySummaryDescription);

        _locationFrom = parent.findViewById (R.id.tvJourneySummaryLocationFrom);
        _locationTo = parent.findViewById (R.id.tvJourneySummaryLocationTo);

        _startDate = parent.findViewById (R.id.tvJourneySummaryStartDate);
        _endDate = parent.findViewById (R.id.tvJourneySummaryEndDate);
        _startTime = parent.findViewById (R.id.tvJourneySummaryStartTime);
        _endTime = parent.findViewById (R.id.tvJourneySummaryEndTime);
        _totalDistance = parent.findViewById (R.id.tvJourneySummaryTotalDistance);
        _totalTime = parent.findViewById (R.id.tvJourneySummaryTotalTime);
        _maxSpeed = parent.findViewById (R.id.tvJourneySummaryMaxSpeed);
        _mediumSpeed = parent.findViewById (R.id.tvJourneySummaryMediumSpeed);
    }

    public void bindView (Journey journey) {
        Location l;

        if (journey.getDescription () != null && !"".equals (journey.getDescription ())) {
            _descriptionRow.setVisibility (View.VISIBLE);
            _description.setText (journey.getDescription ());
        }
        else {
            _descriptionRow.setVisibility (View.GONE);
        }

        getStartDate ().setText (FormatHelper.formatDate (_ctx, journey.getJouneyDate ()));
        getStartTime ().setText (FormatHelper.formatTime (_ctx, journey.getJouneyDate ()));

        l = journey.getStartLocation ();
        _locationFrom.setText (AddressHelper.getLocationAddressDescription (_ctx, l));

        l = journey.getEndLocation ();
        _locationTo.setText (AddressHelper.getLocationAddressDescription (_ctx, l));
        if (l != null) {
            getEndDate ().setText (FormatHelper.formatDate (_ctx, l.getLocationTimeAsDate ()));
            getEndTime ().setText (FormatHelper.formatTime (_ctx, l.getLocationTimeAsDate ()));
        }
        else {
            getEndDate ().setText ("");
            getEndTime ().setText ("");
        }

        getTotalDistance ().setText (FormatHelper.formatDistance (journey.computeTotalDistance (_ctx)));
        getTotalTime ().setText (FormatHelper.formatDuration (journey.computeTotalTime (_ctx)));
        getMaxSpeed ().setText (FormatHelper.formatSpeed (journey.computeMaxSpeed ()));
        getMediumSpeed ().setText (FormatHelper.formatSpeed (journey.computeMediumSpeed ()));
    }

    public TextView getStartDate () {
        return _startDate;
    }

    public TextView getStartTime () {
        return _startTime;
    }

    public TextView getEndDate () {
        return _endDate;
    }

    public TextView getEndTime () {
        return _endTime;
    }

    public TextView getTotalDistance () {
        return _totalDistance;
    }

    public TextView getTotalTime () {
        return _totalTime;
    }

    public TextView getMaxSpeed () {
        return _maxSpeed;
    }

    public TextView getMediumSpeed () {
        return _mediumSpeed;
    }
}