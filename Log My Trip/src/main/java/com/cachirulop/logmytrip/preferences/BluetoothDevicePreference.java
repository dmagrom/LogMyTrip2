package com.cachirulop.logmytrip.preferences;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.AttributeSet;

import androidx.core.app.ActivityCompat;
import androidx.preference.MultiSelectListPreference;

import com.cachirulop.logmytrip.R;

import java.util.Set;

public class BluetoothDevicePreference
        extends MultiSelectListPreference {

    public BluetoothDevicePreference (Context context, AttributeSet attrs) {
        super (context, attrs);

        CharSequence[] entries;
        CharSequence[] entryValues;
        BluetoothManager manager;
        BluetoothAdapter adapter;

        manager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        adapter = manager.getAdapter ();
        if ((adapter != null) &&
            (ActivityCompat.checkSelfPermission (context, Manifest.permission.BLUETOOTH_CONNECT) ==
             PackageManager.PERMISSION_GRANTED)) {
            Set<BluetoothDevice> pairedDevices;
            int i;

            pairedDevices = adapter.getBondedDevices ();
            entries = new CharSequence[pairedDevices.size ()];
            entryValues = new CharSequence[pairedDevices.size ()];
            i = 0;
            for (BluetoothDevice dev : pairedDevices) {
                entries[i] = dev.getName ();
                if (entries[i].toString ().equals ("")) {
                    entries[i] = dev.getAddress ();
                }

                entryValues[i] = dev.getAddress ();

                i++;
            }
        }
        else {
            entries = new CharSequence[1];
            entryValues = new CharSequence[1];
            entries[0] = context.getText (R.string.pref_bluetoothNotFound);
            entryValues[0] = entries[0];
        }

        setEntries (entries);
        setEntryValues (entryValues);
    }
}
